#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<string.h>

using namespace std;

struct nod {
	string data;
   	struct nod *next;
   	struct nod *previous;
}*start, *last;
int count = 0;
class circulardoublylist {
   public:
      nod *create_node(string);
      void insert_begin(string);
      void insert_end(string);
      void insert_pos(int, string);
      void delete_pos(int);
      void update(int, string);
      void display();
      circulardoublylist() {
         start = NULL;
         last = NULL;
      }
};
/*int main() {
	int c;
   circulardoublylist cdl;
   while (1){
      cout<<"1.Insert at Beginning"<<endl;
      cout<<"2.Insert at End"<<endl;
      cout<<"3.Insert at Position"<<endl;
      cout<<"4.Delete at Position"<<endl;
      cout<<"5.Update Node"<<endl;
      cout<<"6.Display List"<<endl;
      cout<<"7.Exit"<<endl;
      cout<<"Enter your choice : ";
      cin>>c;
      switch(c) {
         case 1:
            cdl.insert_begin();
         break;
         case 2:
            cdl.insert_end();
         break;
         case 3:
            cdl.insert_pos();
         break;
         case 4:
            cdl.delete_pos();
         break;
         case 5:
            cdl.update();
         break;
         case 6:
            cdl.display();
         break;
         case 7:
            exit(1);
         default:
            cout<<"Wrong choice"<<endl;
      }
   }
   return 0;
}*/
nod* circulardoublylist::create_node(string v) {
   count++;
   struct nod *t;
   t = new(struct nod);
   t->data = v;
   t->next = NULL;
   t->previous = NULL;
   return t;
}
void circulardoublylist::insert_begin(string v) {
   struct nod *t;
   t = create_node(v);
   if (start == last && start == NULL) {
      cout<<"Element inserted in empty list"<<endl;
      start = last = t;
      start->next = last->next = NULL;
      start->previous = last->previous = NULL;
   } else {
      t->next = start;
      start->previous = t;
      start = t;
      start->previous = last;
      last->next = start;
      cout<<"Element inserted"<<endl;
   }
}
void circulardoublylist::insert_end(string v) {
   struct nod *t;
   t = create_node(v);
   if (start == last && start == NULL) {
      cout<<"data successfully added! "<<endl;
      start = last = t;
      start->next = last->next = NULL;
      start->previous = last->previous= NULL;
      return;
   }
   
      last->next= t;
      t->previous= last;
      last = t;
      start->previous = last;
      last->next= start;
      cout<<"data successfully added! "<<endl;
}
void circulardoublylist::insert_pos(int pos, string v) {
   int i;
   struct nod *t, *s, *ptr;
   t = create_node(v);
   if (start == last && start == NULL) {
      if (pos == 1) {
         start = last = t;
         start->next = last->next = NULL;
         start->previous = last->previous = NULL;
      } else {
         cout<<"Position out of range"<<endl;
         count--;
         return;
      }
   } else {
      if (count < pos) {
         cout<<"Position out of range"<<endl;
         count--;
         return;
      }
      s = start;
      for (i = 1;i <= count;i++) {
         ptr = s;
         s = s->next;
         if (i == pos - 1) {
            ptr->next = t;
            t->previous= ptr;
            t->next= s;
            s->previous = t;
            cout<<"Element inserted"<<endl;
            break;
         }
      }
   }
}
void circulardoublylist::delete_pos(int pos) {
   int i;
   nod *ptr, *s;
   if (start == last && start == NULL) {
      cout<<"List is empty, nothing to delete"<<endl;
      return;
   }
   if (count < pos) {
      cout<<"Position out of range"<<endl;
      return;
   }
   s = start;
   if(pos == 1) {
      count--;
      last->next = s->next;
      s->next->previous = last;
      start = s->next;
      free(s);
      cout<<"Data Deleted"<<endl;
      return;
   }
   for (i = 0;i < pos - 1;i++ ) {
      s = s->next;
      ptr = s->previous;
   }
   ptr->next = s->next;
   s->next->previous = ptr;
   if (pos == count) {
      last = ptr;
   }
   count--;
   free(s);
   cout<<"Data Deleted"<<endl;
}
void circulardoublylist::update(int pos, string v) {
	int i;
   if (start == last && start == NULL) {
      cout<<"The List is empty, nothing to update"<<endl;
      return;
   }
   struct nod *s;
   if (count < pos) {
      cout<<"Position out of range"<<endl;
      return;
   }
	s = start;
   if (pos == 1) {
      s->data = v;
      cout<<"data Updated"<<endl;
   }
	for (i=0;i < pos - 1;i++) {
      	s = s->next;
   	}
   	s->data = v;
   	cout<<"Data Updated"<<endl;	
   
}
void circulardoublylist::display() {
   int i = 1;
   struct nod *s;
   if (start == last && start == NULL) {
      cout<<"The List is empty, nothing to display"<<endl;
      return;
   }
   s = start;
   for (i;i <= count;i++) {
      cout<< i << ". " <<s->data << endl;
      s = s->next;
   }
}
#include <iostream>
#include "CircularDoublyList.h"
#include <string.h>

using namespace std;

void print();
void errorcatch();
string songs[] = {"WhereverYouWillGo-TheCalling", "Perfect-SimplePlan", "FallForYou-SecondHandSerenade", "YourCall-SecondHandSerenade", 
"WelcomeToTheBlackParade-MyChemicalRomance", "IDon'tLoveYou-MyChemicalRomance", "Helena-MyChemicalRomance", "SugarWe'reGoingDown-FallOutBoys",
"HighHopes-PanicAtTheDisco", "BalladofMonalisa-PanicAtTheDisco"};
int main(){
	//for switch statements
	int dec, x, y;
	//for while loop
	int z=1;
	//for user inputs
	int pos;
	string title;
	
	circulardoublylist playlist;
	
	while(1)
	{
		cout << "Song Library: " << endl;
		print();
		cout << "Playlist: " << endl;
		playlist.display();
		
		cout << "\nMenu: " << endl
			<< "(1) Add Music" << endl
			<< "(2) Edit recently added Music" << endl
			<< "(3) Delete recently added Music" << endl
			<< "(4) View Playlist" << endl
			<< "(5) Exit" << endl
			<< "Your response: ";
		cin >> dec;
		errorcatch();
		system("CLS");
		switch(dec){
			case 1:
				cout << "Press (1) to add from the song library or (2) to add new music." << endl;
				cout << "Your response: " << endl;
				cin >> x;
				errorcatch();
				system("CLS");
				switch(x){
					case 1:
						cout << "Song Library: " << endl;
						print();
						cout << "Enter position of the song you would like to add: ";
						cin >> pos;
						errorcatch();
						playlist.insert_begin(songs[pos-1]);
						system("CLS");
						break;
					case 2:
						cout << "Enter the title of the song you would like to add with no spaces(ex. WhereverYouWillGo-TheCalling): ";
						cin >> title;
						errorcatch();
						playlist.insert_begin(title);
						system("CLS");
						break;
					default:
						cout << "Invalid input" << endl;
						system("PAUSE");
						system("CLS");
						break;
				}
				break;
			case 2:
				cout << "Playlist: " << endl;
				playlist.display();	
				cout << "Enter the new title: ";
				cin >> title;
				errorcatch();
				playlist.update(1, title);
				system("CLS");
				break;
			case 3:
				playlist.delete_pos(1);
				system("pause");
				system("CLS");
				break;
			case 4:
				playlist.display();
				system("pause");
				system("CLS");
				break;
			case 5:
				cout<<"Program Terminated" << endl;
				system("PAUSE");
				return 0;
				break;
			default:
				cout<<"Invalid Input" << endl;
				system("PAUSE");
				break;
		}
	}
}

void print(){
	for(int i = 0; i<=9; i++)
	{
		cout << i+1 << ". " << songs[i]<< endl;
	}
}

void errorcatch(){
	if(cin.fail()){
		cin.clear();
		cin.ignore();
		cout << "Invalid Input!" << endl;
		system("PAUSE");
		system("CLS");
		exit(0);
	}
}
